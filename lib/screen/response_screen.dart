import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zhongyingparentpickup/constant/app_constant.dart';
import 'package:zhongyingparentpickup/constant/application.dart';
import 'package:zhongyingparentpickup/cubit/call_cubit.dart';
import 'package:zhongyingparentpickup/cubit/call_state.dart';
import 'package:zhongyingparentpickup/model/call_student.dart';
import 'package:zhongyingparentpickup/screen/scan_qr_screen.dart';
import 'package:zhongyingparentpickup/util/alert_util.dart';
import 'package:zhongyingparentpickup/util/shared_pref_util.dart';
import 'package:zhongyingparentpickup/widget/icon_button_widget.dart';

class ResponseScreen extends StatefulWidget {
  const ResponseScreen(this.card, {Key key}) : super(key: key);

  final String card;

  @override
  _ResponseScreenState createState() => _ResponseScreenState();
}

class _ResponseScreenState extends State<ResponseScreen> {
  final CallCubit _cubit = CallCubit();
  List<Student> _student = [];
  List<Family> _family = [];

  @override
  void initState() {
    SharedPrefUtil.getString(PrefKey.URL).then((value) {
      if (value != null) {
        setState(() {
          application.url = value;
        });
      }
    });
    _cubit.callStudent(widget.card, 0);
    _cubit.listen((state) {
      if (state is CallStateError) {
        return AlertUtil.showErrorDialog(
          context,
          state.message,
          onPressed: () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => ScanQrScreen(),
          )),
        );
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: BlocBuilder(
        cubit: _cubit,
        builder: (context, state) {
          return Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.card,
                    style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 8.0),
                  _buildParent(),
                  const SizedBox(height: 16.0),
                  _buildChild(),
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButton: _buildButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: AppColor.primaryColor,
      leading: IconButtonWidget(
        icon: const Icon(Icons.arrow_back, color: Colors.white),
        onPressed: () => Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => ScanQrScreen(),
        )),
      ),
    );
  }

  Widget _buildParent() {
    return BlocBuilder(
      cubit: _cubit,
      builder: (context, state) {
        if (state is CallStateSuccess) {
          _family = state.response.families;
          return Wrap(
              alignment: WrapAlignment.center,
              direction: Axis.horizontal,
              children: _family
                  .map((e) => Card(
                        elevation: 4.0,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        child: Container(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
                          margin: const EdgeInsets.all(8.0),
                          child: CachedNetworkImage(
                            imageUrl: e.image,
                            width: 150.0,
                            height: 180.0,
                            fit: BoxFit.fill,
                            errorWidget: (context, url, error) {
                              return Opacity(opacity: 0.2, child: Image.asset(AppImage.logo, width: 60.0));
                            },
                            placeholder: (context, url) {
                              return Opacity(opacity: 0.2, child: Image.asset(AppImage.logo, width: 60.0));
                            },
                          ),
                        ),
                      ))
                  .toList());
        }
        return const SizedBox();
      },
    );
  }

  Widget _buildChild() {
    return BlocBuilder(
      cubit: _cubit,
      builder: (context, state) {
        if (state is CallStateSuccess) {
          _student = state.response.students;
          return Wrap(
            alignment: WrapAlignment.center,
            direction: Axis.horizontal,
            runSpacing: 10.0,
            spacing: 10.0,
            children: _student
                .map(
                  (e) => Card(
                    elevation: 4.0,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                    child: Column(
                      children: [
                        Container(
                          width: 150.0,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
                          margin: const EdgeInsets.all(8.0),
                          child: CachedNetworkImage(
                            imageUrl: e.image,
                            width: 150.0,
                            height: 180.0,
                            fit: BoxFit.fill,
                            errorWidget: (context, url, error) {
                              return Opacity(opacity: 0.2, child: Image.asset(AppImage.logo, width: 60.0));
                            },
                            placeholder: (context, url) {
                              return Opacity(opacity: 0.2, child: Image.asset(AppImage.logo, width: 60.0));
                            },
                          ),
                        ),
                        Text(e.studentName),
                        const SizedBox(height: 4.0),
                      ],
                    ),
                  ),
                )
                .toList(),
          );
        }
        return AlertUtil.showLoading(color: AppColor.primaryColor);
      },
    );
  }

  Widget _buildButton() {
    return BlocBuilder(
      cubit: _cubit,
      builder: (context, state) {
        return FloatingActionButton.extended(
          onPressed: () {
            _cubit.callStudent(widget.card, 1).whenComplete(
                  () => Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => ScanQrScreen(),
                  )),
                );
          },
          label: state is CallStateLoading ? AlertUtil.showLoading() : const Text('    Submit    '),
          backgroundColor: AppColor.primaryColor,
        );
      },
    );
  }
}
