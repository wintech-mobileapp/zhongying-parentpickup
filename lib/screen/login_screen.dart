import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zhongyingparentpickup/constant/app_constant.dart';
import 'package:zhongyingparentpickup/constant/application.dart';
import 'package:zhongyingparentpickup/cubit/login_cubit.dart';
import 'package:zhongyingparentpickup/cubit/login_state.dart';
import 'package:zhongyingparentpickup/screen/scan_qr_screen.dart';
import 'package:zhongyingparentpickup/util/alert_util.dart';
import 'package:zhongyingparentpickup/util/shared_pref_util.dart';
import 'package:zhongyingparentpickup/widget/icon_button_widget.dart';

import '../constant/app_constant.dart';
import '../util/alert_util.dart';
import '../util/shared_pref_util.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _key = GlobalKey<FormState>();
  final _key2 = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _urlController = TextEditingController();
  final LoginCubit _cubit = LoginCubit();
  String configUrl = '';

  @override
  void initState() {
    SharedPrefUtil.getString(PrefKey.URL).then((value) {
      if (value != null) {
        setState(() {
          configUrl = value;
        });
      }
    });
    _cubit.listen((state) {
      if (state is LoginStateSuccess) {
        FocusScope.of(context).unfocus();
        _usernameController.clear();
        _passwordController.clear();
        _urlController.clear();
        _urlController.text = configUrl;
        _showMyDialog(context);
      }
      if (state is LoginStateError) {
        AlertUtil.showErrorDialog(context, state.message);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          appBar: _buildAppBar(),
          backgroundColor: Colors.white,
          body: BlocBuilder(
            cubit: _cubit,
            builder: (context, state) {
              return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: SingleChildScrollView(
                      child: Form(
                        key: _key,
                        child: Column(
                          children: [
                            _buildLogo(),
                            const SizedBox(height: 32.0),
                            _textFormFieldCard(),
                            const SizedBox(height: 16.0),
                            _textFormFieldPass(),
                            if (state is LoginStateLoading)
                              AlertUtil.showLoading(color: AppColor.primaryColor)
                            else
                              const SizedBox(),
                            const SizedBox(height: 16.0),
                            _buildLoginButton(),
                          ],
                        ),
                      ),
                    ),
                  ));
            },
          )),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: AppColor.primaryColor,
      leading: IconButtonWidget(
        icon: const Icon(Icons.arrow_back, color: Colors.white),
        onPressed: (){
          FocusScope.of(context).unfocus();
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => ScanQrScreen(),
          ));
        }
      ),
    );
  }

  Widget _buildLogo() {
    return Image.asset(
      AppImage.ZHLogo,
      width: 180.0,
    );
  }

  Widget _textFormFieldCard() {
    return TextFormField(
      controller: _usernameController,
      validator: (text) {
        if (text.isEmpty) return 'Username is Required';
        return null;
      },
      onFieldSubmitted: (value) {
        FocusScope.of(context).nextFocus();
      },
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.person),
        hintText: 'Username',
      ),
    );
  }

  Widget _textFormFieldPass() {
    return TextFormField(
      controller: _passwordController,
      validator: (text) {
        if (text.isEmpty) return 'Password is Required';
        return null;
      },
      onFieldSubmitted: (value) {
        FocusScope.of(context).unfocus();
      },
      obscureText: true,
      textInputAction: TextInputAction.done,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.vpn_key),
        hintText: 'Password',
      ),
    );
  }

  Widget _textFormFieldUrl() {
    return TextFormField(
      controller: _urlController,
      validator: (text) {
        if (text.isEmpty) return 'Please enter your url';
        return null;
      },
      autofocus: true,
      onFieldSubmitted: (value) {
        FocusScope.of(context).unfocus();
      },
      maxLines: 4,
      textInputAction: TextInputAction.done,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        hintText: 'Enter url',
      ),
    );
  }

  Widget _buildLoginButton() {
    return BlocBuilder(
      cubit: _cubit,
      builder: (context, state) {
        return FlatButton(
          onPressed: () {
            FocusScope.of(context).unfocus();
            if (!_key.currentState.validate()) return;
            _cubit.login(_usernameController.text, _passwordController.text);
          },
          child: state is LoginStateLoading
              ? AlertUtil.showLoading()
              : const Text('Login', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
          color: AppColor.primaryColor,
          textColor: Colors.white,
        );
      },
    );
  }

  Widget _buildSaveButton() {
    return BlocBuilder(
      cubit: _cubit,
      builder: (context, state) {
        return FlatButton(
          onPressed: () {
            FocusScope.of(context).unfocus();
            if (!_key2.currentState.validate()) return;
            SharedPrefUtil.setString(PrefKey.URL, _urlController.text);
            setState(() {
              configUrl = _urlController.text;
            });
            SharedPrefUtil.getString(PrefKey.URL).then((value) {
              if (value != null) {
                setState(() {
                  application.url = value;
                });
              }
            });
            Navigator.of(context).pop();
          },
          child: const Text('Save', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
          color: AppColor.primaryColor,
          textColor: Colors.white,
        );
      },
    );
  }

  Future _showMyDialog(BuildContext context) {
    _urlController.text = configUrl;
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(16.0),
          content: SingleChildScrollView(
            child: Form(
              key: _key2,
              child: ListBody(
                children: [
                  SizedBox(width: MediaQuery.of(context).size.width, child: _textFormFieldUrl()),
                  const SizedBox(height: 16.0),
                  _buildSaveButton(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
