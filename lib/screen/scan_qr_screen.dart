import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zhongyingparentpickup/constant/app_constant.dart';
import 'package:zhongyingparentpickup/constant/application.dart';
import 'package:zhongyingparentpickup/screen/login_screen.dart';
import 'package:zhongyingparentpickup/screen/response_screen.dart';
import 'package:zhongyingparentpickup/util/alert_util.dart';
import 'package:zhongyingparentpickup/util/shared_pref_util.dart';
import 'package:zhongyingparentpickup/widget/icon_button_widget.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class ScanQrScreen extends StatefulWidget {
  @override
  _ScanQrScreenState createState() => _ScanQrScreenState();
}

class _ScanQrScreenState extends State<ScanQrScreen> {
  Barcode result;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  void reassemble() {
    Platform.isAndroid ? controller.pauseCamera() : controller.resumeCamera();
    super.reassemble();
  }

  @override
  void initState() {
    SharedPrefUtil.getString(PrefKey.URL).then((value) {
      if (value != null) {
        setState(() {
          application.url = value;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildQrView(context),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: AppColor.primaryColor,
      centerTitle: true,
      title: Image.asset(AppImage.logo, width: 120.0),
      actions: [
        IconButtonWidget(
            icon: const Icon(Icons.settings),
            color: Colors.white,
            onPressed: () {
              controller.dispose();
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ));
            })
      ],
    );
  }

  Widget _buildQrView(BuildContext context) {
    final scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 250.0
        : 300.0;
    return NotificationListener<SizeChangedLayoutNotification>(
        onNotification: (notification) {
          Future.microtask(() => controller?.updateDimensions(qrKey, scanArea: scanArea));
          return false;
        },
        child: SizeChangedLayoutNotifier(
            key: const Key('qr-size-notifier'),
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                borderColor: AppColor.primaryColor,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: scanArea,
              ),
            )));
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.first.then((value) {
      if (value != null) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => ResponseScreen(value.code),
        ));
      } else
        AlertUtil.showErrorDialog(context, 'QR Error!');
    });
  }
}
