class CallStudentResponse {
  CallStudentResponse({
    this.code,
    this.description,
    this.data,
  });

  factory CallStudentResponse.fromJson(dynamic json) => CallStudentResponse(
        code: json['code'] as int,
        description: json['description'].toString(),
        data: CallStudent.fromJson(json['data']),
      );

  int code;
  String description;
  CallStudent data;
}

class CallStudent {
  CallStudent({
    this.students,
    this.families,
  });

  factory CallStudent.fromJson(dynamic json){
    final List<Student> listStudent = [];
    final List<Family> listFamily = [];
    for(final item in json['students']){
      listStudent.add(Student.fromJson(item));
    }
    for(final item in json['families']){
      listFamily.add(Family.fromJson(item));
    }
    return CallStudent(
      students: listStudent,
      families: listFamily,
    );
  }

  List<Student> students;
  List<Family> families;
}
class Student {
  Student({
    this.studentId='',
    this.studentCode='',
    this.studentName='',
    this.image='',
  });



  factory Student.fromJson(dynamic json) => Student(
    studentId: json['studentid'].toString(),
    studentCode: json['student_code'].toString(),
    studentName: json['student_name'].toString(),
    image: json['image'].toString(),
  );

  String studentId;
  String studentCode;
  String studentName;
  String image;
}

class Family {
  Family({
    this.familyId = '',
    this.familyCode = '',
    this.familyName = '',
    this.familyCard = '',
    this.relative = '',
    this.image = '',
  });



  factory Family.fromJson(dynamic json) => Family(
        familyId: json['familyid'].toString(),
        familyCode: json['family_code'].toString(),
        familyName: json['family_name'].toString(),
        familyCard: json['family_card'].toString(),
        relative: json['relative'].toString(),
        image: json['image'].toString(),
      );
  
  String familyId;
  String familyCode;
  String familyName;
  String familyCard;
  String relative;
  String image;
}


