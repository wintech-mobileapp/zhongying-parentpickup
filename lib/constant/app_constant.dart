import 'package:flutter/material.dart';

mixin AppColor {
  static const Color primaryColor = Color(0xFFC70000);
}

mixin AppImage {
  static String ZHLogo = 'assets/images/zhong_ying_logo_no_bg.png';
  static String logo = 'assets/images/logo_appbar.png';
}

