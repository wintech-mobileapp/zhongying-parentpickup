import 'package:flutter/material.dart';

class Application {
  ScanCallBack scanCallBack;

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  bool isNetworkConnected = true;
  String url;
}

final Application application = Application();

typedef ScanCallBack = void Function();
