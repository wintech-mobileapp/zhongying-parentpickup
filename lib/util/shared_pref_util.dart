import 'package:shared_preferences/shared_preferences.dart';

mixin SharedPrefUtil {
  static Future<bool> setString(PrefKey key, String value) async {
    final pref = await SharedPreferences.getInstance();
    return await pref.setString(key.toString(), value);
  }

  static Future<String> getString(PrefKey key) async {
    final pref = await SharedPreferences.getInstance();
    return pref.getString(key.toString()) ?? '';
  }

  static Future<bool> clear() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return await pref.clear();
  }
}

enum PrefKey { URL }
