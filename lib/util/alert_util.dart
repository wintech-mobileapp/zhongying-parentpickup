import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

mixin AlertUtil {
  static Widget showLoading({Color color = Colors.white}) => Center(
      heightFactor: 0,
      child: Platform.isAndroid
          ? CircularProgressIndicator(
              strokeWidth: 2.0,
              valueColor: AlwaysStoppedAnimation<Color>(color),
            )
          : const CupertinoActivityIndicator());

  static void showErrorDialog(BuildContext context, String message,
          {VoidCallback onPressed}) =>
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            titlePadding: EdgeInsets.zero,
            contentPadding: EdgeInsets.zero,
            buttonPadding: EdgeInsets.zero,
            actionsPadding: EdgeInsets.zero,
            title: Container(
                padding: const EdgeInsets.all(16.0),
                width: MediaQuery.of(context).size.width,
                child:
                    Center(child: Image.asset('assets/images/error.png', width: 50.0))),
            content: Container(
                padding: const EdgeInsets.all(16.0),
                width: MediaQuery.of(context).size.width,
                child: Text(message, textAlign: TextAlign.center)),
            actions: <Widget>[
              Container(
                clipBehavior: Clip.none,
                color: Colors.red,
                width: MediaQuery.of(context).size.width,
                child: TextButton(
                  child: const Text(
                    'Dismiss',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (onPressed != null) onPressed();
                  },
                ),
              ),
            ],
          );
        },
      );
}
