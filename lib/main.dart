import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:zhongyingparentpickup/constant/app_constant.dart';
import 'package:zhongyingparentpickup/constant/application.dart';
import 'package:zhongyingparentpickup/screen/scan_qr_screen.dart';
import 'package:zhongyingparentpickup/util/alert_util.dart';
import 'package:zhongyingparentpickup/util/shared_pref_util.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  StreamSubscription<ConnectivityResult> _subscription;

  @override
  void initState() {
    _initConnectivity();
    SharedPrefUtil.getString(PrefKey.URL).then((value) {
      if (value != null) {
        setState(() {
          application.url = value;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  void _initConnectivity() {
    Connectivity().checkConnectivity().then(
          (result) => _checkNetworkConnection(result, true),
        );
    _subscription = Connectivity().onConnectivityChanged.listen(
          (event) => _checkNetworkConnection(event, false),
        );
  }

  void _checkNetworkConnection(ConnectivityResult result, bool withBinding) {
    application.isNetworkConnected = result != ConnectivityResult.none;
    if (result == ConnectivityResult.none && !withBinding) {
      AlertUtil.showErrorDialog(
        application.navigatorKey.currentContext,
        'No internet connection!',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        appBarTheme: const AppBarTheme(color: AppColor.primaryColor),
        primarySwatch: Colors.red,
        inputDecorationTheme: InputDecorationTheme(
          isDense: true,
          enabledBorder:
              OutlineInputBorder(borderSide: const BorderSide(), borderRadius: BorderRadius.circular(8.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
        ),
        buttonTheme: ButtonThemeData(
          height: 48.0,
          minWidth: double.infinity,
          padding: const EdgeInsets.all(8.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
      ),
      home: ScanQrScreen(),
    );
  }
}
