import 'dart:io';

import 'package:dio/dio.dart';
import 'package:zhongyingparentpickup/network/api/api_end_point.dart';
import 'package:zhongyingparentpickup/constant/application.dart';


mixin APIService {
  static Future<Response> request(
    String url,
    DioMethods method, {
    Map<String, dynamic> param,
    FormData formData,
  }) async {
    if (!application.isNetworkConnected) throw 'No internet connection!';
    final dio = Dio(BaseOptions(
      headers: _generateHeaders(null),
      contentType: 'application/x-www-form-urlencoded',
    ));
    Future<Response> response;
    switch (method) {
      case DioMethods.POST:
        response = dio.post(url, data: param ?? formData);
        break;
      case DioMethods.GET:
        response = dio.get(url, queryParameters: param);
        break;
      case DioMethods.PUT:
        response = dio.put(url, data: param);
        break;
      case DioMethods.DELETE:
        response = dio.delete(url, data: param);
        break;
    }
    return response.whenComplete(() => response).catchError((error) => throw 'Somethings went wrong!');
  }
  static Map<String, dynamic> _generateHeaders(String token) => {
    if (token != null) HttpHeaders.authorizationHeader: 'Bearer $token'
  };
}
