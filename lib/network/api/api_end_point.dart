
import 'package:zhongyingparentpickup/constant/application.dart';

class APIEndPoint {
  APIEndPoint._internal();

  static final APIEndPoint _instance = APIEndPoint._internal();

  static APIEndPoint get instance => _instance;

  final String _apiBaseUrl = 'http://zyis.855school.com/zhongying/pickup/v1';

  String get login => '$_apiBaseUrl/user/login';

  String get call => application.url;
}

enum DioMethods { POST, GET, PUT, DELETE }
