import 'package:zhongyingparentpickup/model/call_student.dart';
import 'package:zhongyingparentpickup/network/datasource/other_datasource.dart';

class OtherRepository {
  OtherRepository() {
    _otherDataSource = OtherDataSource();
  }

  OtherDataSource _otherDataSource;

  Future<void> login(String username, String password) async {
    final response = await _otherDataSource.login(username, password);
    if (response.data['code'] != 200) throw response.data['description'];
  }

  Future<CallStudentResponse> callStudent(String familyCard, int submit) async {
    final response = await _otherDataSource.callStudent(familyCard, submit);
    if (response.data['code'] == 200) return CallStudentResponse.fromJson(response.data);
    throw response.data['description'];
  }
}
