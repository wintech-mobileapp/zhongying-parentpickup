import 'package:dio/dio.dart';
import 'package:zhongyingparentpickup/network/api/api_end_point.dart';
import 'package:zhongyingparentpickup/network/api/api_service.dart';

class OtherDataSource {
  Future<Response> login(String username, String password) async {
    return APIService.request(APIEndPoint.instance.login, DioMethods.POST, param: {
      'username': username,
      'password': password,
    });
  }

  Future<Response> callStudent(String familyCard, int submit) async {
    return APIService.request(APIEndPoint.instance.call, DioMethods.POST, param: {
      'family_card': familyCard,
      'submit': submit,
    });
  }
}
