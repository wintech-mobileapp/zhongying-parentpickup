import 'package:flutter/material.dart';

class IconButtonWidget extends IconButton {
  const IconButtonWidget({
    Icon icon,
    double splashRadius = 20.0,
    Color color = Colors.white,
    VoidCallback onPressed,
  }) : super(
    icon: icon,
    color: color,
    onPressed: onPressed,
    splashColor: Colors.transparent,
    splashRadius: splashRadius,
  );
}