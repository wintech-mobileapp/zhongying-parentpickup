import 'package:zhongyingparentpickup/model/call_student.dart';

abstract class CallState {}

class CallStateInit extends CallState {}

class CallStateLoading extends CallState {}

class CallStateError extends CallState {
  CallStateError(this.message);

  final String message;
}

class CallStateSuccess extends CallState {
  CallStateSuccess(this.response);

  CallStudent response;
}
