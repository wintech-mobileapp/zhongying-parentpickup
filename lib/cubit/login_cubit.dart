import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zhongyingparentpickup/cubit/login_state.dart';
import 'package:zhongyingparentpickup/network/repository/other_repository.dart';

class LoginCubit extends Cubit<LoginState>{
  LoginCubit():super(LoginStateInit()){
    _otherRepo = OtherRepository();
  }
  OtherRepository _otherRepo;

  Future<void> login(String username, String password)async{
    try{
      await _otherRepo.login(username, password);
      emit(LoginStateLoading());
      emit(LoginStateSuccess());
    }catch(error){
      emit(LoginStateError(error.toString()));
    }
  }
}