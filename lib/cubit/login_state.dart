abstract class LoginState {}

class LoginStateInit extends LoginState {}

class LoginStateLoading extends LoginState {}

class LoginStateError extends LoginState {
  LoginStateError(this.message);

   final String message;
}

class LoginStateSuccess extends LoginState {}
