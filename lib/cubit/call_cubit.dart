import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zhongyingparentpickup/cubit/call_state.dart';
import 'package:zhongyingparentpickup/network/repository/other_repository.dart';

class CallCubit extends Cubit<CallState> {
  CallCubit() : super(CallStateInit()) {
    _otherRepo = OtherRepository();
  }

  OtherRepository _otherRepo;

  Future<void> callStudent(String familyCard, int submit) async {
    try {
      final res = await _otherRepo.callStudent(familyCard, submit);
      emit(CallStateLoading());
      emit(CallStateSuccess(res.data));
    } catch (error) {
      emit(CallStateError(error.toString()));
    }
  }
}
